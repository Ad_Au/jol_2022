# Manifeste politique de l'Agora (JeuxOnline)

Collaboration des membres de l'Agora autour de l'écriture d'un manifeste politique, de manière à compiler les conclusions des différents débats lorsqu'un consensus semble s'être dégagé.